#!/bin/bash
BINDIR=/projects/nanodata/falcon-bin
IFS='
'
directory="${1%/}"
for avi in $(ls -1 ${directory}/*.avi); do
  sbatch -t "00:30:00" -J "encodeAvi" -o "${avi}.log" ${BINDIR}/ffmpeg.slurm "$avi" "$2"
done

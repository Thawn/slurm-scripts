#!/bin/bash
BINDIR=/projects/nanodata/falcon-bin
IFS='
'
directory="${1%/}"
for file in $(ls -c1 ${directory}/*.avi); do
  sbatch -t "00:30:00" -J "avirecode" -o "${file%avi}log" ${BINDIR}/ffmpeg.slurm "$file"
done

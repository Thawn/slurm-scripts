#!/bin/bash
description="Recombines data and eval folders that were sorted into subdirectories with sort.sh"
usage="$0 <parent directory where the data should be combined>"
if [ -d "$1" ]; then
  dir=${1%/}
  mv -i "$dir"/*/*.{nd2,tif,tiff,stk} "$dir"/
  mkdir -p "$dir/eval"
  mv -i "$dir"/*/eval/* "$dir"/eval/
else
  echo "$description"
  echo "$usage"
fi

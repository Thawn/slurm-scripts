#!/bin/bash
BINDIR=/projects/nanodata/falcon-bin
NumCores=10
WallTime="02:00:00"
echo Args:"$@".
delete=0
ARGS=()
for arg in $@; do
  if [[ "$arg" == "NumCores" ]]; then
    delete=1
    echo $arg
  elif [[ "$arg" == "WallTime" ]]; then
    delete=1
    echo $arg
  elif [ $delete -eq 0 ]; then
    ARGS+=("$arg")
  else
    if [[ "$arg" == "NumCores" ]]; then
      NumCores=$arg
    elif [[ "$arg" == "WallTime" ]]; then
      WallTime="$arg"
    fi
    delete=0
  fi
done
if [ ${#ARGS[@]} -eq 0 ]; then
  buffer=''
else
  buffer=' '
fi
sbatch -t 00:05:00 -o "$(pwd)".log "${BINDIR}"/matlab.slurm "${BINDIR}"/serverMultiMultiSim "$(pwd)" WallTime "$WallTime" BinaryPath "/projects/nanodata/falcon-bin/serverMultiSim" NumCores $NumCores Iterations $NumCores$buffer"${ARGS[@]}"

#!/bin/bash
BINDIR=/projects/nanodata/falcon-bin
IFS='
'
directory="${1%/}"
cd "$directory"
for dir in $(ls -d1 "${directory}"/movie*); do
  if [ -d "$dir" ]; then
    srun -t "00:10:00" rm -r "$dir"&
  fi
done

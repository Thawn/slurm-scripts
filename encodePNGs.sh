#!/bin/bash
BINDIR=/projects/nanodata/falcon-bin
IFS='
'
directory="${1%/}"
for dir in $(ls -d1 ${directory}/movie*); do
  if [ -d "$dir" ]; then
    sbatch -t "00:30:00" -J "encodePNG" -o "${dir}.log" -c 12 ${BINDIR}/ffmpeg_png.slurm "$dir" "$2"
  fi
done

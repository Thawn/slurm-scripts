#!/bin/bash
if [ "x$1" == "x" ]; then
  echo "usage: $0 <path/to/dir/that/contains/your/data> [walltime (01:00:00] [number-of-CPUs]"
else
  source /etc/profile
# Source global definitions
  if [ -f /etc/bashrc ]; then
        . /etc/bashrc
  fi
  BINDIR=/projects/nanodata/falcon-bin
  cd $BINDIR
  oldIFS=$IFS
  IFS='
'
  rootdir=${1%/}
  dirs=$( ls -d1 ${rootdir}/*/ )
  echo "---"
  echo "Starting jobs"
  echo "---"
  counter=1
  for dir in $dirs; do
    if grep -q "results\|done" <<< "$dir"; then
      echo "ignoring $dir"
    else
      "${BINDIR}/startSimulation.sh" "$dir" "${@:2}"
      sleep 1 #we need to wait at least 1 s in between jobs to make sure that in each job the random number generator gets initialized with a different value.
    fi
  done
fi


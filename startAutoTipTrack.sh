#!/bin/bash
if [ "x$1" == "x" ]; then
  echo "usage: $0 <path/to/dir/that/contains/your/data>"
else
  source /etc/profile
# Source global definitions
  if [ -f /etc/bashrc ]; then
        . /etc/bashrc
  fi
  BINDIR=/projects/nanodata/falcon-bin
  cd $BINDIR
  oldIFS=$IFS
  IFS='
'
  if [ "x$2" == "x" ]; then
    wtime='01:00:00'
  else
    wtime=$2
  fi
  if [ "x$3" == "x" ]; then
    numCores=12
  else
    numCores=$3
  fi
  echo "====================="
  echo "using $numCores cpus"
  rootdir=${1%%/}
  dirnames=$( ls -d1 ${rootdir}/*/ )
  echo "---"
  echo "Submitting jobs"
  echo "---"
  for dir in $dirnames; do
    if grep -q "_mcrCache" <<< "$dir"; then
      #if the directory is a matlab runtime cache directory, we ignore it
      echo "Ignoring MCR cache directory: $dir"
    elif grep -q "ignore" <<< "$dir"; then
      #if the directory is called ignore, then we ignore it
      echo "Ignoring directory named ignore: $dir"
    else
      dir="${dir%%/}"
      jobname="AutoTipTrack_$( dirname $dir )"
      jobExecutable="${BINDIR}/evaluateManyExperiments"
      sbatch -t $wtime -J "$jobname" -o "${dir}.log" -c $numCores --mem-per-cpu=5000 matlab.slurm "$jobExecutable" "$dir" "Gui" "false" "NumCPUs" "$numCores"
    fi
  done
fi


#!/bin/bash
if [ "x$1" == "x" ]; then
  echo "usage: $0 <path/to/dir/that/contains/your/data> [walltime (01:00:00] [number-of-CPUs]"
else
  source /etc/profile
# Source global definitions
  if [ -f /etc/bashrc ]; then
        . /etc/bashrc
  fi
  BINDIR=/projects/nanodata/falcon-bin
  cd $BINDIR
  oldIFS=$IFS
  IFS='
'
  if [ "x$2" == "x" ]; then
    wtime='01:00:00'
  else
    wtime=$2
  fi
  if [ "x$3" == "x" ]; then
    numCores=1
  else
    numCores=$3
  fi
  echo "====================="
  echo "using $numCores cpus"
  rootdir=${1%/}
  rm ${rootdir}/._*.mat
  filenames=$( ls -d1 ${rootdir}/*.{mat,DXF} )
  echo "---"
  echo "Submitting jobs"
  echo "---"
  counter=1
  for file in $filenames; do
    if grep -q "arguments.mat" <<< "$file"; then
      echo "ignoring $file"
    else
      jobname="simulate_$(printf %04d $counter)"
      counter=$(( $counter + 1 ))
      jobExecutable="${BINDIR}/simulateOnServer"
      sbatch -t $wtime -J "$jobname" -o "${file}.log" -c $numCores --mem-per-cpu=5000 matlab.slurm "$jobExecutable" "$file" "$numCores"
    fi
  done
fi


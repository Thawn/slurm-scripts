#!/bin/bash
####### Usage ########
#
# example code to start the script:
## sbatch -t $wallTime -J "$jobName" -o "${dir}.log" -c $numCores --mem-per-cpu=5000 matlab.slurm "$pathToMatlabExecutable" "$dir" "${otherMatlabArguments[@]}"
#
# For the matlabCleanup.slurm script, it is important, that the working
# directory ($dir without trailing slash) is passed as the first argument
# after the executable (the second argument in total), otherwise it does
# not find the log files to check for success, and does not know where to
# put its own log files (that way all log files end up in one location).
#
# $dir is therefore also the first parameter, that gets passed to the matlab
# scripts. Therefore the matlab source must account for this. So far,
# all my matlab code needed this parameter anyways. However, if this parameter
# is not needed for your matlab code, your startup function can have one
# empty parameter as the first parameter. For example:
#
# function startup(~,varargin)
#
# The matlab scripts also need to display "Success!" or "All done!" at the
# end of execution so that the cleanup script can check for successful
# execution. For example:
#
# disp('Success!')
#
######################

#Set up the Matlab environment
MCRROOT="/sw/apps/mcr/v93"
echo "------------------------------------------"
echo Setting up environment variables
echo ---
source "${MCRROOT}/mcrenv"
echo LD_LIBRARY_PATH is ${LD_LIBRARY_PATH};
#Set up the matlab cache directory on the nodes' local ssd on /tmp
mcrCache="/tmp/korten/${SLURM_JOB_ID}/"
mkdir -p "$mcrCache"
MCR_CACHE_ROOT="$mcrCache"
export MCR_CACHE_ROOT;
#Copy the executable from our bin directory to the nodes' ssd on /tmp
BINDIR=/projects/nanodata/falcon-bin
cd $BINDIR
filename=$( basename "$1" )
exe="$mcrCache$filename"
cp "$1" "$exe"
#start the cleanup batch job this will make sure cleanup also happens if the current job was cancelled it can also be used to restart jobs that have timed out
sbatch -t "00:10:00" -o "${2}_cleanup.log" --dependency=afterany:$SLURM_JOB_ID ${BINDIR}/matlabCleanup.slurm "$@" "$SLURMD_NODENAME"
sleep 10 #wait to make sure copying the executable has finished, sometimes this can take a while if load on the project space is high
#Run the actual matlab script
"$exe" "${@:2}"
sleep 10 #wait a little to make sure all matlab processes have finished and all temporary file handles are released
# clean up the temporary directory
rm -rf "$mcrCache"

#!/bin/bash
usage="$0 debug compound_name starting_well_number starting_concentration concentration_unit [starting_concentration_2 unit2]
if debug is less than 1, files will be renamed, otherwise target filenames will be echoed"
if [ -z ${1+x} ]; then
  echo "$usage"
else
shopt -s extglob
debug=$1
compound=$2
n_start=$3
n_end=$( printf "%03d" $(( $n_start + 23 )) )
unit=$5
c=1
if [ -z ${6+x} ]; then
for run in {Run1Pos1,Run2Pos1}; do
  conc1=$( printf "%07.3f" $4)
  for n in $( seq -w $n_start $n_end); do
    if [ $debug -lt 1 ]; then
        if ! [ -d ${run}/${compound} ]; then
	    mkdir ${run}/${compound}
	fi
        mv ${run}/2017-08-11-Ilumina_frag_serial_dilution${n}.nd2 ${run}/${compound}/${compound}_${conc1}_${unit}_${n}.nd2
    else
        echo ${compound}_${conc1}_${unit}_${n}.nd2
    fi
    n=$(printf "%03d" $(( 24 + ${n_start##+(0)} + ${n_end##+(0)} - ${n##+(0)} )))
    if [ $debug	-lt 1 ]; then
        mv ${run}/2017-08-11-Ilumina_frag_serial_dilution${n}.nd2 ${run}/${compound}/${compound}_${conc1}_${unit}_${n}.nd2
    else
       	echo ${compound}_${conc1}_${unit}_${n}.nd2
    fi
    if [ $(( $c % 2 )) -lt 1 ];then
	conc1=$( printf "%07.3f" $(bc -l <<< "scale=3; $conc1/1.5" ) )
    fi
    c=$(( $c+1 ))
  done
done
else
unit2=$7
for run in {Run1Pos1,Run2Pos1}; do
  conc1=$( printf "%07.3f" $4)
  conc2=$( printf "%07.3f" $6)
  for n in $( seq -w $n_start $n_end); do
    if [ $debug -lt 1 ]; then
        if ! [ -d ${run}/${compound} ]; then
	    mkdir ${run}/${compound}
	fi
        mv ${run}/2017-08-11-Ilumina_frag_serial_dilution${n}.nd2 ${run}/${compound}/${compound}_${conc1}_${unit}_${conc2}_${unit2}_${n}.nd2
    else
        echo ${compound}_${conc1}_${unit}_${conc2}_${unit2}_${n}.nd2
    fi
    n=$(printf "%03d" $(( 24 + ${n_start##+(0)} + ${n_end##+(0)} - ${n##+(0)} )))
    if [ $debug	-lt 1 ]; then
        mv ${run}/2017-08-11-Ilumina_frag_serial_dilution${n}.nd2 ${run}/${compound}/${compound}_${conc1}_${unit}_${conc2}_${unit2}_${n}.nd2
    else
        echo ${compound}_${conc1}_${unit}_${conc2}_${unit2}_${n}.nd2
    fi
    if [ $(( $c % 2 )) -lt 1 ];then
	conc1=$( printf "%07.3f" $(bc -l <<< "scale=3; $conc1/1.5" ) )
	conc2=$( printf "%07.3f" $(bc -l <<< "scale=3; $conc2/1.5" ) )
    fi
    c=$(( $c+1 ))
  done
done
fi
fi

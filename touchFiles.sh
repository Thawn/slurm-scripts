#!/bin/bash
h=22
min=10
sec=21
for file in $(ls -1 *.tif); do
  touch -t 1214$h$min.$sec $file
  toAdd=$(( 10#$h * 3600 + 10#$min * 60 + 10#$sec + 97))
  h=$(printf %02d $(( $toAdd / 3600 )) )
  toAdd=$(printf %02d $(( $toAdd % 3600 )) )
  min=$(printf %02d $(( $toAdd / 60)) )
  sec=$(printf %02d $(( $toAdd % 60 )) )
  echo "$h:$min:$sec"
done
  
